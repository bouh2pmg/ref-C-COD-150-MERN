import React from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Signin from './auth/signin';
import Signup from './auth/signup';

export default function NavBar() {
	return (
		<Router>
			<div>
			<nav className="navbar navbar-dark bg-primary fixed-top">
				<Link className="navbar-brand" to="/">Your Blogs</Link>
				<Link to="/signin">Sigin</Link>
				<Link to="/signup">Sigup</Link>
			</nav>
			<Route exact path="/" />
			<Route path="/signin" component={Signin} />
			<Route path="/signup" component={Signup} />
			</div>
		</Router>
	);
}