import React, { Component } from "react";
import { Alert, Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import axios from "axios"
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';

import "./auth.css";

export default class Sigin extends Component {
	static propTypes = {
		cookies: instanceOf(Cookies).isRequired
	      };
	constructor(props) {
		super(props);

		const { cookies } = props;		
		this.state = {
			username: "",
			password: "",
			error: ""
		};
	}

	validateForm() {
		return this.state.username.length > 0 && this.state.password.length > 0;
	}

	handleChange = event => {
		this.setState({
			[event.target.id]: event.target.value
		});
	}

	handleSubmit = event => {
		const { cookies } = this.props;		
		event.preventDefault();
		console.log("submitted");
		axios.post("http://127.0.0.1:6969/api/Users/login", {"username": this.state.username, "password": this.state.password}).then( (resp) => {
			console.log(resp);
			//cookies.set('token_id', , { path: '/' });			
			this.props.history.push('/' + this.state.username);
		})
		.catch( (err) => {
			this.setState({"error": err.response.data.error.message});
		})
	}

	render() {
		const error = this.state.error.length > 0 ? <Alert bsStyle="warning">{this.state.error}</Alert> : "";
		return (
			<div className="Login">
				<form onSubmit={this.handleSubmit}>
				<FormGroup controlId="username" bsSize="large">
						<ControlLabel>Username</ControlLabel>
						<FormControl
							autoFocus
							type="text"
							value={this.state.username}
							onChange={this.handleChange}
						/>
					</FormGroup>
					<FormGroup controlId="password" bsSize="large">
						<ControlLabel>Password</ControlLabel>
						<FormControl
							value={this.state.password}
							onChange={this.handleChange}
							type="password"
						/>
					</FormGroup>
					<Button
						block
						bsSize="large"
						disabled={!this.validateForm()}
						type="submit"
					>
						Login
          				</Button>
				</form>

				<div class="Login error">{error}</div>
			</div>
		);
	}
}