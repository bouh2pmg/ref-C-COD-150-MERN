import React, { Component } from "react";
import { Alert, Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import axios from "axios";
import "./auth.css";

export default class Signup extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: "",
			username: "",
			password: "",
			confirmPass: "",
			error: "",
		};
	}

	validateForm() {
		return this.state.email.length > 0 && this.state.username.length > 0 && this.state.password.length > 0 && this.state.password === this.state.confirmPass;
	}

	handleChange = event => {
		this.setState({
			[event.target.id]: event.target.value
		});
	}

	handleSubmit = event => {
		event.preventDefault();
		console.log("submitted");
		axios.post("http://127.0.0.1:6969/api/Users", {"email": this.state.email, "username": this.state.username, "password": this.state.password}).then(response => {
			this.props.history.push('/signin');
		})
		.catch(err => {
			console.log(err);
			this.setState({"error": err.response.data.error.message});
		})
	}

	render() {
		const error = this.state.error.length > 0 ? <Alert bsStyle="warning">{this.state.error}</Alert> : "";
		return (
			<div className="Login">
				<form onSubmit={this.handleSubmit}>
				<FormGroup controlId="username" bsSize="large">
						<ControlLabel>Username</ControlLabel>
						<FormControl
							autoFocus
							type="text"
							value={this.state.username}
							onChange={this.handleChange}
						/>
					</FormGroup>
					<FormGroup controlId="email" bsSize="large">
						<ControlLabel>Email</ControlLabel>
						<FormControl
							type="email"
							value={this.state.email}
							onChange={this.handleChange}
						/>
					</FormGroup>
					<FormGroup controlId="password" bsSize="large">
						<ControlLabel>Password</ControlLabel>
						<FormControl
							value={this.state.password}
							onChange={this.handleChange}
							type="password"
						/>
					</FormGroup>
					<FormGroup controlId="confirmPass" bsSize="large">
						<ControlLabel>Confirm password</ControlLabel>
						<FormControl
							value={this.state.confirmPass}
							onChange={this.handleChange}
							type="password"
						/>
					</FormGroup>
					<Button
						block
						bsSize="large"
						disabled={!this.validateForm()}
						type="submit"
					>
						Register
          				</Button>
				</form>

				<div className="Login error">{error}</div>
			</div>
		);
	}
}