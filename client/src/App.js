import React, { Component } from 'react';
import NavBar from './NavBar';
import { CookiesProvider } from 'react-cookie';

class App extends Component {
  render() {
    return (
      <CookiesProvider>
        <NavBar />
      </CookiesProvider>
    );
  }
}

export default App;